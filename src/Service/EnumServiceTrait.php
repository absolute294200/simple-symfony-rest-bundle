<?php
/**
 * Created by rest-bundle.
 * User: ssp
 * Date: 14.09.16
 * Time: 16:36.
 */
namespace SimpleSymfony\Symfony\RestBundle\Service;

use SimpleSymfony\Symfony\RestBundle\Repository\EntityRepository;

trait EnumServiceTrait
{
    /**
     * @var EntityRepository
     */
    protected $repository;
}

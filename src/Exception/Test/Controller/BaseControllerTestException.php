<?php

namespace SimpleSymfony\Symfony\RestBundle\Exception\Test\Controller;

/**
 * Class BaseControllerTestException.
 */
class BaseControllerTestException extends \RuntimeException
{
}

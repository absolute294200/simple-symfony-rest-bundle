<?php

namespace SimpleSymfony\Symfony\RestBundle\Handler;

/**
 * Interface ProcessorInterface.
 */
interface ProcessorInterface extends ErrorInterface
{
}

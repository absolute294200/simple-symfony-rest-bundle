<?php

namespace SimpleSymfony\Symfony\RestBundle\Entity;

/**
 * Interface EntityInterface.
 */
interface EntityInterface extends NullableInterface
{
}

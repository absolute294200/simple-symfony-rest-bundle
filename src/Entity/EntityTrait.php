<?php

namespace SimpleSymfony\Symfony\RestBundle\Entity;

/**
 * Class EntityTrait.
 */
trait EntityTrait
{
    use NullableTrait;
}

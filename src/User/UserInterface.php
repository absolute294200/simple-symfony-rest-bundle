<?php
/**
 * Created by rest-bundle.
 * User: ssp
 * Date: 13.09.16
 * Time: 11:52.
 */
namespace SimpleSymfony\Symfony\RestBundle\User;

use SimpleSymfony\Symfony\RestBundle\Entity\EntityInterface;

interface UserInterface extends EntityInterface
{
}

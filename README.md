# Simple Symfony Rest Bundle

This Bundle extends friendsofsymfony/rest-bundle to easy develop rest applications.

## Requirements

The following versions of PHP are supported.

* PHP 7.0

## Install

Via Composer

``` bash
$ composer require simple-symfony/rest-bundle:3.*
```

## Documentation
[Soft Deletable](http://atlantic18.github.io/DoctrineExtensions/doc/softdeleteable.html)


## Configuration

* Optional: Soft Deletable 
app/config.yaml

``` yaml
doctrine:
    orm:
        filters:
            softdeleteable:
                class: Gedmo\SoftDeleteable\Filter\SoftDeleteableFilter
                enabled: true
```

## Usage

## Contributing


## License
